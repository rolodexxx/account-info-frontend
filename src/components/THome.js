import React, { useEffect, useState } from "react";
import axios from "axios";
import Card from "./common/Card";
import Styles from "./components.module.css";

const THome = () => {
  const [accounts, setAccounts] = useState([]);

  useEffect(() => {
    const getAccounts = async () => {
      let res = await axios.get("http://localhost:8000/transformer/users/");
      setAccounts(res.data);
    };
    getAccounts();
  }, []);

  const renderAccounts = accounts.map((item) => (
    <Card
      id={item.id}
      name={item.name}
      desc={item.description0}
      key={item.id}
    />
  ));

  return (
    <div>
      <h1 className={Styles.my5}>Transformer Accounts Book</h1>
      <div className={`${Styles.cardGrid}`}>{renderAccounts}</div>
    </div>
  );
};

export default THome;
