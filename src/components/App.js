import React from "react";
import Header from "./common/Header";
import { Route, Switch } from "react-router-dom";
import Home from "./Home";
import THome from "./THome";

import CreateUser from "./CreateUser";
const App = () => {
  return (
    <div>
      <Header />
      <div className="ui container">
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/transformer/users" component={THome} exact />
          <Route path="/signup" component={CreateUser} />
        </Switch>
      </div>
    </div>
  );
};

export default App;
