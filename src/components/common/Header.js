import React from "react";
import { Link } from "react-router-dom";
const Header = () => {
  return (
    <div>
      <div className="ui secondary pointing menu">
        <Link to="/" className="item">
          Home
        </Link>
        <Link to="/signup" className="item">
          Create account
        </Link>
        <div className="right menu">
          <Link className="ui item">View codebase</Link>
        </div>
      </div>
    </div>
  );
};

export default Header;
