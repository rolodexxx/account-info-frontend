import React from "react";
const Card = ({ name, id, desc }) => {
  return (
    <div className="ui card">
      <div className="content">
        <div className="header">{name}</div>
        <div className="meta">#{id}</div>
        <div className="description">
          <p>{desc}</p>
        </div>
      </div>
    </div>
  );
};

export default Card;
