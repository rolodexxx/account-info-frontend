import React, { useEffect, useState } from "react";
import axios from "axios";
import Card from "./common/Card";
import Styles from "./components.module.css";

const Home = () => {
  const [accounts, setAccounts] = useState([]);

  useEffect(() => {
    const getAccounts = async () => {
      let res = await axios.get("http://localhost:8000/users/");
      setAccounts(res.data.data);
    };
    getAccounts();
  }, []);

  const renderAccounts = accounts.map((item) => (
    <Card id={item.id} name={item.name} desc={item.desc} key={item.id} />
  ));

  return (
    <div>
      <h1 className={Styles.my5}>Account Book</h1>
      <div className={`${Styles.cardGrid}`}>{renderAccounts}</div>
    </div>
  );
};

export default Home;
